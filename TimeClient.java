import java.io.*;
import java.net.*;
/**
*
* TimeClient class
*
* connect to server and get current date
*
* ----- compile -----
*
* javac TimeClient.java
*
* ----- run example -----
*
* java TimeClient localhost 5555
*
*/
class TimeClient{
    public static void main(String args[]){
        String hostname;
        int port;
        Socket socket;
        InputStream input;
        BufferedReader reader;
        String time;
        if(args.length != 2){ // need couple of command line arguments, host name and port number
            return;
        }
        hostname=args[0];
        port=Integer.parseInt(args[1]);
        try{
            socket=new Socket(hostname,port);
            input=socket.getInputStream();
            reader=new BufferedReader(new InputStreamReader(input));
            time=reader.readLine(); // read time from server
            System.out.println(time);
        }catch(UnknownHostException uhe){
            uhe.printStackTrace();
        }catch(IOException ioe){
            ioe.printStackTrace();
        }
    }
}
