import java.io.*;
import java.net.*;
import java.util.Date;
/**
*
* TimeServer class
*
* accept client connection and print current date
*
* ----- DESCRIPTION ServerSocket -----
*
* It is used to create a server socket and bind it to some port. Next, listen for client connection and accept.
* Next, read data from client using InputStream. At the same time print data to client using OutputStream.
* As a last step, close connection to client.
*
* ----- DESCRIPTION Socket -----
*
* A Socket object actually handles communication using input and output streams. This is true both for server and 
* client.
*
* ----- compile -----
*
* javac TimeServer.java
*
* ----- run example -----
*
* java TimeServer 5555
*
* ----- references -----
*
* https://www.codejava.net/java-se/networking/java-socket-server-examples-tcp-ip
*
*/
class TimeServer{
    public static void main(String args[]){
        int port;
        ServerSocket srsocket;
        Socket socket;
        OutputStream output;
        PrintWriter printer;
        if(args.length != 1){ // need one command line argument, port number
            return;
        }
        port=Integer.parseInt(args[0]);
        try{
            srsocket=new ServerSocket(port);
            System.out.println("Server started on port "+port);
            for(;true;){
                socket=srsocket.accept();
                System.out.println("Another client connected.");
                output=socket.getOutputStream(); // stream to send data to client
                printer=new PrintWriter(output,true);
                printer.println(new Date().toString());
            }
        }catch(IOException ioe){
            ioe.printStackTrace();
        }
    }
}
